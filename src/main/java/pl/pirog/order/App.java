package pl.pirog.order;

import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingOptionException;

public class App {

    public static void main(String[] args) throws Exception {
        try {
            AppParams params = AppParams.parseArgs(args);
            Selector selector = new Selector(params.getPersons());
            if (params.isClean()) {
                DataCleaner cleaner = new DataCleaner();
                cleaner.clean();
            } else if (!params.getPersons().isEmpty()){
                String chosenPerson = selector.choose();
                new DataFile().persist(chosenPerson);
                System.out.printf("Chosen person: %s", chosenPerson);
            } else {
                throw new MissingOptionException("You should provide at least one option!");
            }
        } catch (MissingOptionException e) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("order", AppParams.getOptions(), true);
        }
    }
}
