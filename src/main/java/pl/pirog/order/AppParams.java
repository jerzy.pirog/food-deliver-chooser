package pl.pirog.order;

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import lombok.Getter;
import org.apache.commons.cli.*;

import java.util.Set;

@Getter
class AppParams {
    private final CommandLine cmd;
    private Set<String> persons;
    private boolean clean;

    public AppParams(CommandLine cmd) {
        this.cmd = cmd;
    }

    @Getter
    private static final Options options = new Options();
    static {
        options.addOption(Option.builder("l")
                .longOpt("list")
                .hasArg()
                .desc("lists persons")
                .build());
        options.addOption(Option.builder("c")
                .longOpt("clean")
                .desc("clean history data")
                .build());

    }

    public static AppParams parseArgs(String[] args) throws ParseException {
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse(options, args);
        AppParams params = new AppParams(cmd);
        params.persons = params.parsePersons();
        params.clean = params.parseClean();
        return params;
    }

    private boolean parseClean() {
        return cmd.hasOption("c");
    }


    private Set<String> parsePersons() {
        if (cmd.getOptionValue("l") == null) return Sets.newHashSet();
        Preconditions.checkArgument(cmd.getOptionValue("l").contains(","), "You should add more persons (via dot sign)");
        String[] persons = cmd.getOptionValue("l").split(",");
        return Sets.newHashSet(persons);
    }
}
