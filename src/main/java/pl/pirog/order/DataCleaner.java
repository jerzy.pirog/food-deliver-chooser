package pl.pirog.order;

import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;

import java.io.File;

class DataCleaner {
    @SneakyThrows
    public void clean() {
        FileUtils.touch(new File("data.test"));
        FileUtils.forceDelete(FileUtils.getFile("data.test"));
        System.out.println("Data cleaned!");
    }
}
