package pl.pirog.order;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.charset.Charset;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;


class DataFile {
    private List<String> lines = new ArrayList<>();
    private boolean added=false;

    public void persist(String chosenPerson) {
        try{
            File data = new File("data.test");
            if (data.exists()) {
                this.lines = FileUtils.readLines(data, Charset.defaultCharset());
                for (ListIterator<String> it = lines.listIterator(); it.hasNext(); ) {
                    String line = it.next();
                    if (line.startsWith(chosenPerson)) {
                        String[] splitted = line.split(";");
                        it.set(splitted[0] + ";" + Instant.now().toString());
                        added = true;
                        break;
                    }
                }
            }
            if (!added) {
                lines.add(chosenPerson + ";" + Instant.now().toString());
            }
            FileUtils.write(data, lines.stream().map(s -> s + System.getProperty("line.separator")).collect(Collectors.joining()), Charset.defaultCharset(), false);
        }catch (Exception e){//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
    }
}
