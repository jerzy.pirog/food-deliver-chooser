package pl.pirog.order;

import com.google.common.base.Strings;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

class Selector {
    private final Set<String> persons;
    private List<String> lines;
    private String chosen;
    private Instant chosenLastActivity;
    private List<String> firstTimePersons = new ArrayList<>();
    public Selector(Set<String> persons) {
        this.persons = persons;
    }

    public String choose() throws IOException {
        File data = new File("data.test");
        firstTimePersons.addAll(persons);
        if (data.exists()) {
            this.lines = FileUtils.readLines(data, Charset.defaultCharset());
            for (String line : lines) {
                String[] lineData = line.split(";");
                Instant actualLastActivity = Instant.parse(lineData[1]);
                firstTimePersons.remove(lineData[0]);
                if (persons.contains(lineData[0]) && (Strings.isNullOrEmpty(chosen) || actualLastActivity.isBefore(chosenLastActivity))) {
                    chosen = lineData[0];
                    chosenLastActivity = actualLastActivity;
                }
            }
        }
        if (firstTimePersons.isEmpty()) {
            return chosen;
        } else {
            System.out.printf("RANDOM MODE! %d\n", firstTimePersons.size());
            Random generator = new Random();
            int randomIndex = generator.nextInt(firstTimePersons.size());
            return firstTimePersons.get(randomIndex);
        }
    }
}
